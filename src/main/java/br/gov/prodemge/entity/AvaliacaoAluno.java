package br.gov.prodemge.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "avaliacao_aluno")
public class AvaliacaoAluno implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer           id;

	@Column
	@Temporal(TemporalType.DATE)
	private Date              data;

	@Column(name = "nota_avaliacao")
	private Integer           notaAvaliacao;

	@Column(name = "nota_aluno")
	private Integer           notaAluno;

	@JoinColumn(name = "aluno_classe_id", referencedColumnName = "id")
	@ManyToOne
	private AlunoClasse       alunoClasse;

	@JoinColumn(name = "bimestre_id", referencedColumnName = "id")
	@ManyToOne
	private Bimestre          bimestre;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Date getData()
	{
		return data;
	}

	public void setData(Date data)
	{
		this.data = data;
	}

	public Integer getNotaAvaliacao()
	{
		return notaAvaliacao;
	}

	public void setNotaAvaliacao(Integer notaAvaliacao)
	{
		this.notaAvaliacao = notaAvaliacao;
	}

	public Integer getNotaAluno()
	{
		return notaAluno;
	}

	public void setNotaAluno(Integer notaAluno)
	{
		this.notaAluno = notaAluno;
	}

	public AlunoClasse getAlunoClasse()
	{
		return alunoClasse;
	}

	public void setAlunoClasse(AlunoClasse alunoClasse)
	{
		this.alunoClasse = alunoClasse;
	}

	public Bimestre getBimestre()
	{
		return bimestre;
	}

	public void setBimestre(Bimestre bimestre)
	{
		this.bimestre = bimestre;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		AvaliacaoAluno other = (AvaliacaoAluno) obj;
		if(id == null)
		{
			if(other.id != null) return false;
		}
		else if(!id.equals(other.id)) return false;
		return true;
	}

}