package br.gov.prodemge.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.prodemge.entity.AlunoClasse;
import br.gov.prodemge.entity.AvaliacaoAluno;
import br.gov.prodemge.entity.Bimestre;
import br.gov.prodemge.entity.Classe;
import br.gov.prodemge.repository.AlunoClasseDao;
import br.gov.prodemge.repository.AvaliacaoAlunoDao;

@RestController
@RequestMapping("avaliacao")
public class AvaliacaoAlunoController extends AbstractController<AvaliacaoAluno, Integer>
{
	@Autowired
	private AlunoClasseDao alunoClasseDao;

	@RequestMapping(value = "/createNewAvaliacao", method = RequestMethod.POST)
	public void saveList(@RequestBody Date data, @RequestBody Integer notaAvaliacao, @RequestBody Integer idClasse,
	   @RequestBody Integer idBimestre)
	{
		List<AlunoClasse> listAlunoClasse = alunoClasseDao.findAllByClasse(new Classe(idClasse));

		List<AvaliacaoAluno> listAvaliacaoAluno = new ArrayList<AvaliacaoAluno>();
		for(AlunoClasse alunoClasse : listAlunoClasse)
		{
			AvaliacaoAluno avaliacaoAluno = new AvaliacaoAluno();
			avaliacaoAluno.setData(data);
			avaliacaoAluno.setAlunoClasse(alunoClasse);
			avaliacaoAluno.setBimestre(new Bimestre(idBimestre));
			avaliacaoAluno.setNotaAvaliacao(notaAvaliacao);
			listAvaliacaoAluno.add(avaliacaoAluno);
		}

		((AvaliacaoAlunoDao) dao).save(listAvaliacaoAluno);
	}

	@RequestMapping("/findAllAvaliacoesByClasse/{id}")
	public List<AvaliacaoAluno> findAllAvaliacoesByClasse(@PathVariable("id") int id)
	{
		return ((AvaliacaoAlunoDao) dao).findAllAvaliacoesByClasse(new Classe(id));
	}
	
	@RequestMapping("/findAllAvaliacoesAlunoByClasse/{id}")
	public List<AvaliacaoAluno> findAllAvaliacoesAlunoByClasse(@PathVariable("id") int id)
	{
		return ((AvaliacaoAlunoDao) dao).findAllAvaliacoesAlunoByClasse(new Classe(id));
	}

}