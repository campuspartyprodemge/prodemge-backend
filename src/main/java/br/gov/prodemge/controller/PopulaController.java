package br.gov.prodemge.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.prodemge.entity.Aluno;
import br.gov.prodemge.entity.AlunoClasse;
import br.gov.prodemge.entity.Bimestre;
import br.gov.prodemge.entity.Classe;
import br.gov.prodemge.entity.Curso;
import br.gov.prodemge.entity.Materia;
import br.gov.prodemge.entity.Professor;
import br.gov.prodemge.entity.Serie;
import br.gov.prodemge.repository.AlunoClasseDao;
import br.gov.prodemge.repository.AlunoDao;
import br.gov.prodemge.repository.BimestreDao;
import br.gov.prodemge.repository.ClasseDao;
import br.gov.prodemge.repository.CursoDao;
import br.gov.prodemge.repository.MateriaDao;
import br.gov.prodemge.repository.ProfessorDao;
import br.gov.prodemge.repository.SerieDao;

@RestController
public class PopulaController
{
	@Autowired
	private BimestreDao    bimestreDao;

	@Autowired
	private SerieDao       serieDao;

	@Autowired
	private ClasseDao       classeDao;
	
	@Autowired
	private CursoDao       cursoDao;

	@Autowired
	private MateriaDao     materiaDao;

	@Autowired
	private AlunoDao       alunoDao;

	@Autowired
	private AlunoClasseDao alunoClasseDao;

	@Autowired
	private ProfessorDao   professorDao;

	@RequestMapping("/populate")
	public String populate()
	{
		String[] bimestres = {"1ª Bimestre", "2ª Bimestre", "3ª Bimestre", "4ª Bimestre"};
		List<Bimestre> listBimestre = new ArrayList<Bimestre>();
		for(int i = 0; i < bimestres.length; i++)
		{
			Bimestre bimestre = new Bimestre(bimestres[i]);
			bimestreDao.save(bimestre);
			listBimestre.add(bimestre);
		}

		String[] series = {"1ª Ano", "2ª Ano", "3ª Ano"};
		List<Serie> listSerie = new ArrayList<Serie>();
		for(int i = 0; i < series.length; i++)
		{
			Serie serie = new Serie(series[i]);
			serieDao.save(serie);
			listSerie.add(serie);
		}

		String[] cursos = {"Matématica"};
		List<Curso> listCurso = new ArrayList<Curso>();
		for(int i = 0; i < cursos.length; i++)
		{
			for(Serie serie : listSerie)
			{
				Curso curso = new Curso(cursos[i], serie);
				cursoDao.save(curso);
				listCurso.add(curso);
			}
		}

		// MATERIAS
		for(Curso curso : listCurso)
		{
			for(int i = 1; i <= 30; i++)
			{
				Materia materia = new Materia("Materia " + i, false, curso);
				materiaDao.save(materia);
			}
		}

		Professor professor = new Professor("Professor Pascal", "111.111.111-11");
		professorDao.save(professor);

		List<Aluno> listAlunos = new ArrayList<Aluno>();
		for(int i = 1; i <= 60; i++)
		{
			Aluno aluno = new Aluno("Aluno " + i);
			alunoDao.save(aluno);
			listAlunos.add(aluno);
		}

		// CLASSES
		List<Classe> listClasse = new ArrayList<Classe>();
		for(Curso curso : listCurso)
		{
			Classe classe = new Classe(professor, curso);
			classeDao.save(classe);
			listClasse.add(classe);
		}

		// ALUNOS CLASSES
		int contClasse = 0, contAluno = 0; 
		for(Aluno aluno : listAlunos)
		{
			if(contAluno == 20 || contAluno == 40){
				contClasse++;
			}
			Classe classe = listClasse.get(contClasse);
			AlunoClasse alunoClasse = new AlunoClasse(aluno, classe);
			alunoClasseDao.save(alunoClasse);
			contAluno++;
		}

		return "Banco gerado com sucesso!";
	}

}
