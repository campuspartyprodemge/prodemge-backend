package br.gov.prodemge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.prodemge.entity.Classe;
import br.gov.prodemge.entity.Professor;
import br.gov.prodemge.repository.ClasseDao;

@RestController
@RequestMapping("professor")
public class ProfessorController extends AbstractController<Professor, Integer>
{
	@Autowired
	private ClasseDao classeDao;

	@RequestMapping("/classe/{id}")
	public List<Classe> findAllClassesByProfessor(@PathVariable("id") int id)
	{
		return classeDao.findAllByProfessor(new Professor(id));
	}
	

}