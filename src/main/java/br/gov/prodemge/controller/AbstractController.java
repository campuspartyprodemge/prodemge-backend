package br.gov.prodemge.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public abstract class AbstractController<T extends Serializable, ID extends Serializable> {
	
	@Autowired
	protected CrudRepository<T, ID> dao;
	
	@RequestMapping("/")
	public Iterable<T> list() {
		return dao.findAll();
	}
	
	@RequestMapping("/{id}")
	public T findById(@PathVariable("id") ID id) {
		return dao.findOne(id);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public T save(@RequestBody T entity) {
		return dao.save(entity);
	}

	@RequestMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") ID id) {
		dao.delete(id);
	}
}
