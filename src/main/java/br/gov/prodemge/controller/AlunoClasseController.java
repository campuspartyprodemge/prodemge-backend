package br.gov.prodemge.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.prodemge.entity.AlunoClasse;
import br.gov.prodemge.entity.Classe;
import br.gov.prodemge.repository.AlunoClasseDao;

@RestController
@RequestMapping("alunoclasse")
public class AlunoClasseController extends AbstractController<AlunoClasse, Integer>{

	@RequestMapping("/findAllByClasse/{id}")
	public List<AlunoClasse> findAllByClasse(@PathVariable("id") int id)
	{
		return ((AlunoClasseDao) dao).findAllByClasse(new Classe(id));
	}
}