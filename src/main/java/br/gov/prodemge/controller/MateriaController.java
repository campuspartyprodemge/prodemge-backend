package br.gov.prodemge.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.prodemge.entity.Curso;
import br.gov.prodemge.entity.Materia;
import br.gov.prodemge.repository.MateriaDao;

@RestController
@RequestMapping("materia")
public class MateriaController extends AbstractController<Materia, Integer>
{

	@RequestMapping("/findByCurso/{id}")
	public List<Materia> findByCurso(@PathVariable("id") int id)
	{
		return ((MateriaDao) dao).findAllByCurso(new Curso(id));
	}

}