package br.gov.prodemge.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.prodemge.entity.PresencaAluno;
import br.gov.prodemge.repository.PresencaAlunoDao;

@RestController
@RequestMapping("presenca")
public class PresencaAlunoController extends AbstractController<PresencaAluno, Integer>
{

	@RequestMapping(value = "/saveList", method = RequestMethod.POST)
	public void saveList(@RequestBody List<PresencaAluno> list)
	{
		((PresencaAlunoDao) dao).save(list);
	}

}