package br.gov.prodemge.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import br.gov.prodemge.entity.Curso;
import br.gov.prodemge.entity.Materia;

@Transactional
public interface MateriaDao extends CrudRepository<Materia, Integer>
{

	public List<Materia> findAllByCurso(Curso curso);

}
