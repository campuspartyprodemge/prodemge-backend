package br.gov.prodemge.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import br.gov.prodemge.entity.Professor;

@Transactional
public interface ProfessorDao extends CrudRepository<Professor, Integer> {
	
}
