package br.gov.prodemge.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.gov.prodemge.entity.AvaliacaoAluno;
import br.gov.prodemge.entity.Classe;

public interface AvaliacaoAlunoDao extends CrudRepository<AvaliacaoAluno, Integer>
{

	@Query("FROM AvaliacaoAluno a JOIN a.alunoClasse c where c.classe = :classe")
	public List<AvaliacaoAluno> findAllAvaliacoesAlunoByClasse(@Param("classe") Classe classe);

	@Query("FROM AvaliacaoAluno a JOIN a.alunoClasse c where c.classe = :classe GROUP BY c.classe")
	public List<AvaliacaoAluno> findAllAvaliacoesByClasse(@Param("classe") Classe classe);

}
