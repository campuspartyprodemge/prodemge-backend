package br.gov.prodemge.repository;

import org.springframework.data.repository.CrudRepository;

import br.gov.prodemge.entity.PresencaAluno;

public interface PresencaAlunoDao extends CrudRepository<PresencaAluno, Integer>{

	
	
}
