package br.gov.prodemge.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import br.gov.prodemge.entity.Classe;
import br.gov.prodemge.entity.Professor;

@Transactional
public interface ClasseDao extends CrudRepository<Classe, Integer> {
	
	public List<Classe> findAllByProfessor(Professor professor);
	
}
