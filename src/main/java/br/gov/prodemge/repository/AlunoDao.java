package br.gov.prodemge.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import br.gov.prodemge.entity.Aluno;

@Transactional
public interface AlunoDao extends CrudRepository<Aluno, Integer> {
	
	public Aluno findByNome(String nome);
	
}
