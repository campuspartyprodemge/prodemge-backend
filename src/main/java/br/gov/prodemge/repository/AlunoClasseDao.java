package br.gov.prodemge.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.gov.prodemge.entity.AlunoClasse;
import br.gov.prodemge.entity.Classe;

public interface AlunoClasseDao extends CrudRepository<AlunoClasse, Integer>{

	public List<AlunoClasse> findAllByClasse(Classe classe);
	
}
