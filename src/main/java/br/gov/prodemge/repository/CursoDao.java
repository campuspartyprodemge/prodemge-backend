package br.gov.prodemge.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import br.gov.prodemge.entity.Curso;

@Transactional
public interface CursoDao extends CrudRepository<Curso, Integer> {
	
}
