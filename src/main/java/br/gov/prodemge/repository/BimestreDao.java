package br.gov.prodemge.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import br.gov.prodemge.entity.Bimestre;

@Transactional
public interface BimestreDao extends CrudRepository<Bimestre, Integer> {
	
}
